<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>RSF</title>

        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">

        <!-- Custom CSS -->
        <link href="../../assets/css/af_main.css" rel="stylesheet">

        <!-- octicons -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/octicons/4.4.0/font/octicons.css" rel="stylesheet">


    </head>

    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <div class="container">
                    <ul class="navbar-nav">
                        <li class="nav-item ">
                            <a class="navbar-brand" href="../../index.php" ><img class="w-25" src="../../images/RSF-sinfondo.png">Hazte socio de RSF</a>                            
                        </li>   
                        <!--<li class="nav-item active">
                            <a class="nav-link" href="../../index.php">Hazte socio de RSF </a>
                        </li>-->                 
                    </ul>
                </div>
            </div>
        </nav>


        <!-- FORMULARIO DE INSERCIÓN -->
        <div class="container" style="margin-top: 30px">
            <form class="form-horizontal" method="post" action="../controllers/insertController.php">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Nombre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre">
                    </div>
                </div>
                <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Localidad</label>
                    <div class="col-sm-10">
                        <textarea type="text" class="form-control" id="localidad" name="localidad" placeholder="Localidad"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="avatar" class="col-sm-2 control-label">URL Foto</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="urlfoto" name="urlFoto" placeholder="Foto">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Crear</button>
                    </div>
                </div>
            </form>
            <!-- FIN DEL FORMULARIO  -->
            <img class="w-25" id="thanksMsg" src="../../images/RSF.png" width="300"/>
            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p> &copy; RSF - 2020</p>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->


        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    </body>

</html>
