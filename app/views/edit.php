<?php
//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/SocioDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Socio.php');


//Compruebo que me llega por GET el parámetro
if (isset($_GET["id"])) {
    $id = $_GET["id"];
    
    $socioDAO = new SocioDAO();
    $socio = $socioDAO->selectById($id);
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Gestión de socios</title>

        <!-- Bootstrap Core CSS -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">


    </head>
    <body>
         <!-- Navigation -->
        <nav class="navbar navbar-light navbar-fixed-top navbar-expand-md bg-faded" role="navigation" style="background-color: #e3f2fd;">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="../../index.php"> <img src="../../assets/img/small-logo.png" alt="" ></a>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">  
                    <a class="navbar-brand" href="../../index.php" ><img class="w-25" src="../../images/RSF-sinfondo.png"></a>
                    <a type="button" class="btn btn-info " href="../views/insert.php">Unirte como socio</a>
                </div>
              </nav>

        <!-- Page Content -->
        <div class="container" style="margin-top: 30px">
            <form class="form-horizontal" method="post" action="../controllers/editController.php">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Nombre</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo $socio->getNombre(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Localidad</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="localidad" name="localidad" placeholder="Localidad" value="<?php echo $socio->getLocalidad(); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="urlPicture" class="col-sm-2 control-label">Url Foto</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="urlFoto" name="urlFoto" placeholder="Url Foto" value="<?php echo $socio->getUrlFoto(); ?>">
                    </div>
                </div>
                
                <input type="hidden" name="id" value="<?php echo $socio->getId(); ?>">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Editar</button>
                    </div>
                </div>
            </form>

            <!-- Footer -->
           <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>&copy; RSF - 2020</p>
                    </div>
                </div>
            </footer>

        </div>
        <!-- /.container -->

        <!-- jQuery -->
        <script src="../../assets/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../../assets/js/bootstrap.min.js"></script>
    </body>

</html>


