<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/SocioDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Socio.php');


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    editAction();
}

function editAction() {
    
    $id = $_POST["id"];
    $nombre = $_POST["nombre"];
    $localidad = $_POST["localidad"];
    $urlFoto = $_POST["urlFoto"];
    
    $socio = new Socio();
    $socio->setId($id);
    $socio->setNombre($nombre);
    $socio->setLocalidad($localidad);
    $socio->setUrlFoto($urlFoto);

    $socioDAO = new SocioDAO();
    $socioDAO->update($socio);

    header('Location: ../../index.php');
}

?>

