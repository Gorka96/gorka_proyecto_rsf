<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/SocioDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Socio.php');


function indexAction() {
    $socioDAO = new SocioDAO();
    return $socioDAO->selectAll();
}

?>