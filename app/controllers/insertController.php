<?php

//Es necesario que importemos los ficheros creados con anterioridad porque los vamos a utilizar desde este fichero.
require_once(dirname(__FILE__) . '/../../persistence/DAO/SocioDAO.php');
require_once(dirname(__FILE__) . '/../../app/models/Socio.php');
require_once(dirname(__FILE__) . '/../../app/models/validations/ValidationsRules.php');



if ($_SERVER["REQUEST_METHOD"] == "POST") {
//Llamo a la función en cuanto se redirija el action a esta página
    createAction();
}

function createAction() {
    // TODOD hacer uso de los valores validados 
    $socio = new Socio();
    $socio->setNombre($_POST["nombre"]);
    $socio->setLocalidad($_POST["localidad"]);
    $socio->setUrlFoto($_POST["urlFoto"]);

    //Creamos un objeto CreatureDAO para hacer las llamadas a la BD
    $socioDAO = new SocioDAO();
    $socioDAO->insert($socio);
    
    header('Location: ../../index.php');
    
}
?>

