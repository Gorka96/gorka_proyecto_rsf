<?php
/**
 * @title: Proyecto integrador Ev01 - Registro en el sistema.
 * @description:  Script PHP para almacenar un nuevo usuario en la base de datos
 *
 * @version    0.1
 *
 * @author ander_frago@cuatrovientos.org
 */

require_once '../templates/header_app.php';
require_once '../persistence/conf/PersistentManager.php';
require_once '../persistence/DAO/UserDAO.php';

$error = $user = $pass = "";

if (isset($_POST['user'])) {
    
  $user=$_POST['user'];
  $pass=$_POST['pass'];
  
  if ($user == "" || $pass == "") {
    $error = "Debes completar todos los campos<br><br>";
  }
  else {
      
     $conection=PersistentManager::getInstance();
     $userDAO = new UserDAO();
    if ($userDAO->checkExists($user,$pass))
    {
      $error = "<span class='error'Usuario/Contraseña invalida</span><br><br>";
    }else{
     $userDAO->insert($user,$pass);
     SessionHelper::setSession($user);
     header('Location: ../index.php');
    }

  }
}
?>

<div class="container">
    <form class="form-horizontal" role="form" method="POST" action="signup.php">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h2>Por favor registrate</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="form-group has-danger">
                    <label class="sr-only" for="user">Usuario:</label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i
                                    class="fa fa-at"></i></div>
                        <input type="text" name="user" class="form-control"
                               id="email"
                               placeholder="Nombre de Usuario" required
                               autofocus>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="sr-only" for="pass">Contraseña:</label>
                    <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div class="input-group-addon" style="width: 2.6rem"><i
                                    class="fa fa-key"></i></div>
                        <input type="password" name="pass"
                               class="form-control" id="password"
                               placeholder="Password" required>
                    </div>
                </div>
            </div>
           
        </div>
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                  <div class="form-control-feedback">
                      <span class="text-danger align-middle">
                     <?php
                         echo $error;
                     ?>
                      </span>
                  </div>
            </div>
        </div>
        <div class="row" style="padding-top: 1rem">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-success"><i class="fa fa-sign-in"></i> Acceder</button>
            </div>
        </div>
    </form>
</div>
