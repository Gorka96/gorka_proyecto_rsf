<?php

class Socio {

    private $id;
    private $nombre;
    private $localidad;
    private $urlFoto;

    public function __construct() {
        
    }

    public function getId() {
        return $this->id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getLocalidad() {
        return $this->localidad;
    }

    public  function getUrlFoto() {
        return $this->urlFoto;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setLocalidad($localidad) {
        $this->localidad = $localidad;
    }

    function setUrlFoto($urlFoto) {
        $this->urlFoto = $urlFoto;
    }

//Función para pintar cada criatura
    function socio2HTML() {
        $result = '<div class="col-sm-4 col-lg-4 col-md-4">';
         $result .= '<div class="card">';
          $result .= ' <img src="'.$this->getUrlFoto().'" alt="">';
            $result .= '<div class="card-body">';
                $result .= '<h4><a href="app/views/edit.php?id='.$this->getId().'">'.$this->getNombre().'</a>';
                $result .= '<a href="app/controllers/deleteController.php?id='.$this->getId().'"> <span class="octicon octicon-trashcan"></span></a>';                    
             $result .= '</h4>';
             $result .= ' <p class="card-text">'.$this->getLocalidad().'</p>';
                $result .= '<a class="btn btn-primary" href="app/views/detail.php?id='.$this->getId().'">Ver más...</a>';
                $result .= '</div>';
                $result .= '</div>';
            $result .= ' </div>';
        
        return $result;
    }
    
    
}
