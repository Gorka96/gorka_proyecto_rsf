<?php
/**
 * @title: Proyecto integrador Ev01 - Página principal
 * @description:  Bienvenida a la aplicación
 *
 * @version    0.1
 *
 * @author ander_frago@cuatrovientos.org
 */
include 'templates/header.php';
require_once(dirname(__FILE__) . '/app/controllers/indexController.php');
$socio = indexAction();
?>

<link rel="stylesheet" href=".\assets\css\bootstrap.css">
<link rel="stylesheet" href=".\assets\css\style.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/octicons/4.4.0/font/octicons.css" rel="stylesheet">

    <div class="jumbotron jumbotron-fluid bg-primary">
        <div id="bienvenida" class="container">  
            <?php 
                  echo "<h1 class='display-3'>Real Sociedad (RSF)</h1>";

                  if ($loggedin) echo "<span class='badge badge-default'> Has iniciado sesión: ".$user."</span>";
                  else           echo "<span class='badge badge-default'> por favor, regístrate o inicia sesión.</span>";
              ?>
        </div>
     </div>
    <div id="carouselExampleIndicators" class="carousel slide w-50" style="left:25%;" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
        <img src="./images/bultzada.jpg" class="d-block w-100" alt="Bultzada">
    </div>
    <div class="carousel-item">
        <img src="images/reale.jpg" class="d-block w-100" alt="Reale Arena">
    </div>
    <div class="carousel-item">
        <img src="./images/RSF.png" class="d-block w-100" alt="RSF">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
        
        <p class="lead" style="padding-top: 1rem">Desde RSF queremos dar la bienvenida a todos los aficionados de la Real Sociedad que visitan la página. </p>
</div>
<div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="jumbotron">
                        <p class="lead">Socios</p>
                        <div class="list-group">
                            <a href="app/views/insert.php" class="list-group-item">Unete!</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                            <img class="img-fluid" src="images/.png" alt="">
                        </div>
                    </div>
                </div>

                <!-- ESTE DIV ES LO QUE VOY A REPETIR MIENTRAS HAYA RESULTADOS -->
                <?php for ($i = 0; $i < sizeof($socio); $i+=3) { ?>
                <div class="row"> 
                <?php
                for ($j = $i; $j < ($i + 3); $j++) {
                   if (isset($socio[$j])) {
                       
                        echo $socio[$j]->socio2HTML();
                    }
                }
                ?>
                   </div> 
                <!-- FIN DEL DIV QUE REPITO -->
                <?php } ?>
            </div>

        </div>
    <footer class="footer bg-primary">
      <div class="container d-flex justify-content-center" style="padding: 1rem">
          <span>Desarrollo Web - 2º DAM / Gorka Hernandez / Instituto 4Vientos</span>
      </div>
    </footer>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>

<script src=".\assets\js\bootstrap.js"></script>

</body>

</html>