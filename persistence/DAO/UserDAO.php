<?php
require 'GenericDAO.php';

class UserDAO extends GenericDAO {

  //Se define una constante con el nombre de la tabla
  const USER_TABLE = 'usuarios';

  public function selectAll() {
    $query = "SELECT * FROM " . UserDAO::USER_TABLE;
    $result = mysqli_query($this->conn, $query);
    $users= array();
    while ($userBD = mysqli_fetch_array($result)) {
      $user = array(
        'id' => $userBD["id"],
        'Nombre' => $userBD["Nombre"],
        'Contraseña' => $userBD["Contraseña"],
      );
      array_push($users, $user);
    }
    return $users;
  }



  public function insert($user, $password) {
    $query = "INSERT INTO " . UserDAO::USER_TABLE .
      " (Nombre,Contraseña) VALUES(?,?)";
    $stmt = mysqli_prepare($this->conn, $query);
    mysqli_stmt_bind_param($stmt, 'ss', $user, $password);
    return $stmt->execute();
  }

  public function checkExists($user, $pass) {
    $query = "SELECT Nombre, Contraseña FROM " . UserDAO::USER_TABLE . " WHERE Nombre=? AND Contraseña=?";
    $stmt = mysqli_prepare($this->conn, $query);
    mysqli_stmt_bind_param($stmt, 'ss', $user, $pass);
    // La consulta se ha lanzado correctamente?? $result=true
    $result = mysqli_stmt_execute($stmt);
    //!IMP¿ Capturar resultado
    $stmt->store_result();
    if($stmt->num_rows >0)
      return true;
    else
      return false;
  }


  public function selectById($id) {
    $query = "SELECT Nombre, Contraseña FROM " . UserDAO::USER_TABLE . " WHERE idUser=?";
    $stmt = mysqli_prepare($this->conn, $query);
    mysqli_stmt_bind_param($stmt, 'i', $id);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $email, $password);

    while (mysqli_stmt_fetch($stmt)) {
      $user = array(
        'id' => $id,
 				'Nombre' => $email,
 				'Contraseña' => $password
 		);
       }

    return $user;
  }

  public function update($id, $email, $password) {
    $query = "UPDATE " . UserDAO::USER_TABLE .
      " SET Nombre=?, Contraseña=?"
      . " WHERE idUser=?";
    $stmt = mysqli_prepare($this->conn, $query);
    mysqli_stmt_bind_param($stmt, 'sssi', $email, $password, $id);
    return $stmt->execute();
  }

  public function delete($id) {
    $query = "DELETE FROM " . UserDAO::USER_TABLE . " WHERE idUser =?";
    $stmt = mysqli_prepare($this->conn, $query);
    mysqli_stmt_bind_param($stmt, 'i', $id);
    return $stmt->execute();
  }

}

?>
