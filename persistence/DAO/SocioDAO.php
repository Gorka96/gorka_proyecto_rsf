<?php

/* Clase UserDAO.php
 * Clase que aplica el Patrón de Diseño DAO para manejar toda la información de un objeto Usuario.
 * author Eugenia Pérez
 * mailto eugenia_perez@cuatrovientos.org
 */
require_once(dirname(__FILE__) . '/../../persistence/conf/PersistentManager.php');

class SocioDAO {

    //Se define una constante con el nombre de la tabla
    const TRIP_TABLE = 'socio';

    //Conexión a BD
    private $conn = null;

    //Constructor de la clase
    public function __construct() {
        $this->conn = PersistentManager::getInstance()->get_connection();
    }

    public function selectAll() {
        $query = "SELECT * FROM " . SocioDAO::TRIP_TABLE;
        $result = mysqli_query($this->conn, $query);
        $socios = array();
        while ($socioBD = mysqli_fetch_array($result)) {

            $socio = new Socio();
            $socio->setId($socioBD["id"]);
            $socio->setNombre($socioBD["nombre"]);
            $socio->setLocalidad($socioBD["localidad"]);
            $socio->setUrlFoto($socioBD["urlFoto"]);
            
            array_push($socios, $socio);
        }
        return $socios;
    }

    public function insert($socio) {
        $query = "INSERT INTO " . SocioDAO::TRIP_TABLE .
                " (nombre, localidad, urlFoto) VALUES(?,?,?)";
        $stmt = mysqli_prepare($this->conn, $query);
        $nombre = $socio->getNombre();
        $localidad = $socio->getLocalidad();
        $urlFoto = $socio->getUrlFoto();
        
        mysqli_stmt_bind_param($stmt, 'sss', $nombre, $localidad, $urlFoto);
        return $stmt->execute();
    }

    public function selectById($id) {
        $query = "SELECT nombre, localidad, urlFoto FROM " . SocioDAO::TRIP_TABLE . " WHERE id=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $nombre, $localidad, $urlFoto);

        $socio = new Socio();
        while (mysqli_stmt_fetch($stmt)) {
            $socio->setId($id);
            $socio->setNombre($nombre);
            $socio->setLocalidad($localidad);
            $socio->setUrlFoto($urlFoto);
       }

        return $socio;
    }

    public function update($socio) {
        $query = "UPDATE " . SocioDAO::TRIP_TABLE .
                " SET nombre=?, localidad=?, urlFoto=?"
                . " WHERE id=?";
        $stmt = mysqli_prepare($this->conn, $query);
        $nombre = $socio->getNombre();
        $localidad= $socio->getLocalidad();
        $urlFoto= $socio->getUrlFoto();
        $id = $socio->getId();
        mysqli_stmt_bind_param($stmt, 'sssi', $nombre, $localidad, $urlFoto, $id);
        return $stmt->execute();
    }
    
    public function delete($id) {
        $query = "DELETE FROM " . SocioDAO::TRIP_TABLE . " WHERE id=?";
        $stmt = mysqli_prepare($this->conn, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        return $stmt->execute();
    }

}

?>
